import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';


class App extends Component {

    constructor(props) {
        super(props)


        this.state = {
            personname: undefined,
            states: undefined,
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    handleInputChange(event) {
        const name = event.target.name;
        const value = event.target.value;

        this.setState({
            [name]: value
        })

    }

    onSubmit(e) {
        e.preventDefault();
        const personname = this.state.personname;
        const states = this.state.states;
        const inputParams = {
            personname: personname,
            states: states,
        }
        this.props.onSubmit(inputParams);
    }

    render() {
        let typeTextField = "TextField";
        let typeDropDown = "DropDown";
        return (

            <div className="App">
                {this.props.config.items[0].type === typeTextField ?
                    <TextField name={this.props.config.items[0].name} label={this.props.config.items[0].label} onChange={this.handleInputChange} value={this.state.personname} /> : null}
                <br />
                {this.props.config.items[1].type === typeDropDown ?
                    <FormControl>
                        <InputLabel htmlFor="state-simple">{this.props.config.items[1].label}</InputLabel>
                        <Select
                            value={this.state.states}
                            onChange={this.handleInputChange}
                            inputProps={{
                                name: 'states',
                                id: 'state-simple',
                            }}
                        >
                            <MenuItem value="">
                               
                            </MenuItem>
                            {this.props.config.items[1].values.map(data =>
                                <MenuItem key={data} value={data}>{data}</MenuItem>
                            )}
                        </Select>
                    </FormControl>
                    : null}
                <br />
                <br />
                <button onClick={this.onSubmit} >Submit</button>
            </div>
        );
    }
}

export default App;
