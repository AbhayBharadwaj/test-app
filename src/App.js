import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import constants from './constants/constants.js';
import Flexi from './views/Flexi.js';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      personNameValue: undefined,
      stateValue: undefined,
    };
    this.onFlexiSubmit = this.onFlexiSubmit.bind(this);

  }

  onFlexiSubmit(inputParams){
    this.setState({
      personNameValue: inputParams.personname,
      stateValue: inputParams.states,
    });
  }

  render() {
    let flexiConfig = constants.flexiConfig;
    return (
      <div className="App">
        <Flexi onSubmit={this.onFlexiSubmit} config={flexiConfig} />
        <br/>
        <p> selected values are: </p>
        {this.state.personNameValue}<br/>
        {this.state.stateValue}

      </div>
    );
  }
}

export default App;
